package com.lohika.training;

import java.io.File;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pgyschuk on 12/19/15.
 */
@Configuration
@ComponentScan("com.lohika.training")
@SpringBootApplication
public class AppConfig {
    private static String AKKA_PORT;

    @Bean(name = "akka-demo1")
    public ActorSystem getActorSystem1() {
        Config config = ConfigFactory.parseFile(new File("src/main/resources/conf"+AKKA_PORT+".config"));
        return ActorSystem.create("akka-demo1", config);
    }

    public static void main(String[] args) throws Exception {
        AKKA_PORT = args[0];
        SpringApplication.run(AppConfig.class, args);
    }
}
