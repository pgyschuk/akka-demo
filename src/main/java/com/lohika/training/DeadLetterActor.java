package com.lohika.training;

import akka.actor.DeadLetter;
import akka.actor.UntypedActor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by pgyschuk on 12/20/15.
 */
public class DeadLetterActor extends UntypedActor {
    private static Log LOG = LogFactory.getLog(DeadLetterActor.class);

    @Override
    public void onReceive(Object message) {
        if (message instanceof DeadLetter) {
            LOG.info("DeadLetterActor handled message: " + message);
        }
    }

    @Override
    public void preStart() throws Exception {
        LOG.info("Actor with name " + getSelf().path().toString() + " created");
        super.preStart();
    }
}