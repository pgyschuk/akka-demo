package com.lohika.training;

import akka.actor.Actor;
import akka.actor.IndirectActorProducer;

/**
 * Created by pgyschuk on 12/19/15.
 */
public class ActorProducer implements IndirectActorProducer {

    @Override
    public Actor produce() {
        DemoActor demoActor = new DemoActor();
        return demoActor;
    }


    @Override public Class<? extends Actor> actorClass() {
        return DemoActor.class;
    }
}
