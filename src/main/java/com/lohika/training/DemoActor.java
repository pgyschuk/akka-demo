package com.lohika.training;

import akka.actor.UntypedActor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by pgyschuk on 12/19/15.
 */
public class DemoActor extends UntypedActor {
    private static Log LOG = LogFactory.getLog(DemoActor.class);

    @Override
    public void onReceive(Object o) throws Exception {
        LOG.info(String.format("Actor %s received message %s", getSelf().path().toString(), o));
    }

    @Override
    public void preStart() throws Exception {
        LOG.info("Actor with name " + getSelf().path().toString() + " created");
        super.preStart();
    }

    @Override public void postStop() throws Exception {
        LOG.info("Actor with name " + getSelf().path().toString() + " stopped");
        super.postStop();
    }
}
