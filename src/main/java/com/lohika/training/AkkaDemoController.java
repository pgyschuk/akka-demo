/**
 * Created by pgyschuk on 12/19/15.
 */
package com.lohika.training;


import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.PoisonPill;
import akka.actor.Props;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AkkaDemoController {
    private static Log LOG = LogFactory.getLog(AkkaDemoController.class);

    @Autowired
    private ActorSystem actorSystem;


    @RequestMapping("/createPathActor")
    @ResponseBody
    public String createPathActor(@RequestParam(name = "a") String actorName) {
        Props actorProperties = Props.create(ActorProducer.class);
        ActorRef actor = actorSystem.actorOf(actorProperties, actorName);
        return actor.path().toString();
    }

    @RequestMapping("/createTypedActor")
    @ResponseBody
    public String createTypedActor() {
        final ActorRef stringHandlerActor = actorSystem.actorOf(Props.create(StringHandlerActor.class));
        actorSystem.eventStream().subscribe(stringHandlerActor, String.class);
        return stringHandlerActor.path().toString();
    }

    @RequestMapping("/createDeadLetterActor")
    @ResponseBody
    public String createDeadLetterActor() {
        final ActorRef deadLetterActor = actorSystem.actorOf(Props.create(DeadLetterActor.class));
        actorSystem.eventStream().subscribe(deadLetterActor, DeadLetter.class);
        return deadLetterActor.path().toString();
    }

    @RequestMapping("/generateActors")
    @ResponseBody
    public void generateActors() {
        Props actorProperties = Props.create(ActorProducer.class);
        for (int i = 1; i < 2500000; i++) {
            actorSystem.actorOf(actorProperties, "test" + i);
        }
    }

    @RequestMapping("/sendPathMessage")
    @ResponseBody
    public void sendRemote(@RequestParam(name = "a") String actorName,
                           @RequestParam(name = "p") String actorPort,
                           @RequestParam(name = "m") String message) {
        ActorSelection actorSelection = actorSystem.actorSelection("akka.tcp://akka-demo1@localhost:" +
                                                                   actorPort + "/user/" + actorName);
        actorSelection.tell(message, ActorRef.noSender());
    }

    @RequestMapping("/sendTypedMessage")
    @ResponseBody
    public void sendTyped(@RequestParam(name = "m") String message) {
        actorSystem.eventStream().publish(message);
    }

    @RequestMapping("/killPathActor")
    @ResponseBody
    public void killActor(@RequestParam(name = "a") String actorName,
                          @RequestParam(name = "p") String actorPort) {
        ActorSelection actorSelection = actorSystem.actorSelection("akka.tcp://akka-demo1@localhost:" +
                                                                   actorPort + "/user/" + actorName);
        actorSelection.tell(PoisonPill.getInstance(), ActorRef.noSender());
    }

}
