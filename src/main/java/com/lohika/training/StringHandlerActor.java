package com.lohika.training;

import akka.actor.UntypedActor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by pgyschuk on 12/20/15.
 */
public class StringHandlerActor extends UntypedActor {

    private static Log LOG = LogFactory.getLog(StringHandlerActor.class);

    @Override
    public void onReceive(Object o) throws Exception {
        LOG.info(String.format("Actor %s received handled message %s", getSelf().path().toString(), o));
    }

    @Override
    public void preStart() throws Exception {
        LOG.info("Actor with name " + getSelf().path().toString() + " created");
        super.preStart();
    }
}
